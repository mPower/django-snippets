import os
from setuptools import setup, find_packages
import snippets

with open(os.path.join(os.path.dirname(__file__), 'README.txt')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name='django-snippets',
    version=snippets.__version__,
    long_description=README,
    author='Mpower',
    author_email='mpower.public@yandex.ru',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Django>=1.6,<1.7',
        'django-mptt>=0.6.0,<0.7'
        ],
    )
