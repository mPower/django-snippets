from __future__ import unicode_literals

from django.db import transaction
from django.contrib import admin
from django.contrib.messages import info
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.core.urlresolvers import reverse
try:
    from django.utils.encoding import force_text
except ImportError:
    # Backward compatibility for Py2 and Django < 1.5
    from django.utils.encoding import force_unicode as force_text

from snippets.conf.models import Setting
from snippets.conf.forms import SettingsForm


csrf_protect_m = method_decorator(csrf_protect)


def admin_url(obj, view):
    return "admin:%s_%s_%s" % (obj._meta.app_label, obj._meta.model_name, view)

class SettingsAdmin(admin.ModelAdmin):
    """
    Admin class for settings model. Redirect add/change views to the list
    view where a single form is rendered for editing all settings.
    """

    class Media:
        css = {"all": ("snippets/css/admin/settings.css",)}

    def changelist_redirect(self):
        changelist_url = reverse(admin_url(Setting, "changelist"), current_app=self.admin_site.name)
        return HttpResponseRedirect(changelist_url)

    def add_view(self, *args, **kwargs):
        return self.changelist_redirect()

    def change_view(self, *args, **kwargs):
        return self.changelist_redirect()

    def has_add_permission(self, request):
        return False

    @csrf_protect_m
    @transaction.atomic
    def changelist_view(self, request, extra_context=None):
        if extra_context is None:
            extra_context = {}
        settings_form = SettingsForm(request.POST or None)
        if settings_form.is_valid():
            settings_form.save()
            info(request, _("Settings were successfully updated."))
            return self.changelist_redirect()
        extra_context["settings_form"] = settings_form
        extra_context["title"] = u"%s %s" % (
            _("Change"), force_text(Setting._meta.verbose_name_plural))
        return super(SettingsAdmin, self).changelist_view(request,
                                                            extra_context)


admin.site.register(Setting, SettingsAdmin)
