from __future__ import unicode_literals
from django.conf import settings as django_settings
from django.core.cache import cache

def cache_installed():
    if django_settings.CACHES and not django_settings.DEBUG:
        return True
    return False

def cache_set(key, value, timeout=None):
    if not timeout:
        timeout = django_settings.CACHE_MIDDLEWARE_SECONDS
    return cache.set(key,value,timeout)

def cache_get(key):
    return cache.get(key)

def settings(request=None):
    """
    Add the settings object to the template context.
    """
    from snippets.conf import settings
    settings_dict = None
    cache_settings = request and cache_installed()
    if cache_settings:
        cache_key = django_settings.CACHE_MIDDLEWARE_KEY_PREFIX + ".snippets-settings"
        settings_dict = cache_get(cache_key)
    if not settings_dict:
        settings.use_editable()
        settings_dict = {}
        for k in settings.all():
            settings_dict[k] = getattr(settings, k, "")
        if cache_settings:
            cache_set(cache_key, settings_dict)
    return {"settings": settings_dict}
