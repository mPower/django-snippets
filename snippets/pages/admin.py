# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __
from django.contrib import admin
from django.core.urlresolvers import reverse
from mptt.admin import MPTTModelAdmin, TreeNodeChoiceField
from snippets.core.admin import MPTTOrderingMixin
from snippets.pages.forms import PageAdminForm
from snippets.pages.models import Page
from snippets.pages.site import site
from snippets.seo.admin import MetaDataInline


class ChoiceFieldPatched(TreeNodeChoiceField):
    def __init__(self, *args, **kwargs):
        kwargs.pop('max_length')
        super(ChoiceFieldPatched, self).__init__(*args, **kwargs)

    def to_python(self, value):
        res = super(ChoiceFieldPatched, self).to_python(value)
        if res:
            return res.path
        return ''


class PageAdmin(MPTTOrderingMixin, MPTTModelAdmin):
    list_display = ('title', 'page_actions', 'page_order', 'page_type', 'page_url', 'is_published', 'menu_list',
                    'in_sitemap', 'pub_date')
    list_display_links = ('title',)
    list_filter = ('is_published', )
    search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('title',)}
    mptt_level_indent = 0
    list_per_page = 1000
    form = PageAdminForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'is_published', 'pub_date', 'expiry_date',
                       'content', 'template', 'pagetype', 'in_menu', 'in_sitemap', 'parent')
        }),
    )
    _add_fields = ('title', 'slug', 'pagetype', 'parent')
    _add_fields_index = ('title', 'pagetype', 'parent')
    _othertype_fields = ('title', 'is_published', 'pagetype', 'in_menu', 'in_sitemap')
    inlines = [MetaDataInline]
    change_list_template = 'admin/snippets/pages/page/change_list.html'

    def page_actions(self, obj):
        info = self.model._meta.app_label, self.model._meta.model_name
        add_child = "<a href='%s?parent=%s'>%s</a>" % (reverse('admin:%s_%s_add' % info), obj.pk, __('Add'))
        return ' '.join((add_child,))
    page_actions.short_description = _('Actions')
    page_actions.allow_tags = True

    def page_order(self, obj):
        info = self.model._meta.app_label, self.model._meta.model_name
        movedown = '&nbsp;&nbsp;&nbsp;'
        moveup = ''
        if obj.get_next_sibling():
            movedown = "<a href='%s'><i class='glyphicon glyphicon-arrow-down'></i></a>" % \
                       (reverse('admin:%s_%s_movedown' % info, args=(obj.pk,)),)
        if obj.get_previous_sibling():
            moveup = "<a href='%s'><i class='glyphicon glyphicon-arrow-up'></i></a>" % \
                     (reverse('admin:%s_%s_moveup' % info, args=(obj.pk,)),)
        return '&nbsp;'.join((movedown, moveup))
    page_order.short_description = _('Order')
    page_order.allow_tags = True

    def menu_list(self, obj):
        if obj.in_menu:
            return True
        return  False
    menu_list.short_description = _('In menu')
    menu_list.boolean = True

    def page_type(self, obj):
        return obj.get_pagetype_display()
    page_type.short_description = _('Page type')

    def page_url(self, obj):
        if obj.redirect:
            return obj.redirect
        return obj.path
    page_url.short_description = _('Path')

    def has_pages(self):
        return self.model.objects.all().count() > 0

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            if not self.has_pages():
                return [(None, {'fields': self._add_fields_index})]
            return [(None, {'fields': self._add_fields})]
        pagetype = obj.pagetype
        if obj and request.POST:
            obj_original = self.get_object(request, obj.pk)
            pagetype = obj_original.pagetype
        if pagetype != '~':
            page_type = site.get(pagetype)
            fields = self._othertype_fields
            if page_type:
                fields += tuple(page_type.fields or ())
            return [(None, {'fields': fields})]
        return super(PageAdmin, self).get_fieldsets(request, obj)

    def get_prepopulated_fields(self, request, obj=None):
        if obj is None:
            return self.prepopulated_fields
        if obj.pagetype != '~':
            return {}
        return self.prepopulated_fields

    def get_inline_instances(self, request, obj=None):
        if obj is None:
            return []
        return super(PageAdmin, self).get_inline_instances(request, obj)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'redirect_to':
            kwargs.update({
                'form_class': ChoiceFieldPatched,
                'widget': ChoiceFieldPatched.widget,
                'queryset': self.model.objects.all(),
                'to_field_name': 'path'
            })
        return super(PageAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def get_form(self, request, obj=None, **kwargs):
        FormClass = super(PageAdmin, self).get_form(request, obj, **kwargs)
        parent = request.GET.get('parent', None)
        if obj is None and parent is None:
            try:
                root = self.model.objects.get(slug='index')
            except self.model.DoesNotExist:
                return FormClass

            class FormClassInitial(FormClass):
                def __init__(self, *args, **kwargs):
                    kwargs.update({'initial': {
                        'parent': root.pk
                    }})
                    super(FormClassInitial, self).__init__(*args, **kwargs)
            return FormClassInitial
        return FormClass

    def save_model(self, request, obj, form, change):
        if not change:
            parent = request.GET.get('parent', None)
            parent_page = None
            if parent:
                try:
                    parent_page = self.model.objects.get(pk=parent)
                except self.model.DoesNotExist:
                    pass
            obj.parent = parent_page
        super(PageAdmin, self).save_model(request, obj, form, change)

    class Media:
        css = {
            'all': ('pages/css/jquery.treetable.css',
                    'pages/css/jquery.treetable.theme.default.css',
                    'bootstrap/css/bootstrap-glyphicons.css',)
        }
        js = ('pages/js/jquery-ui-1.10.4.custom.min.js',
              'pages/js/jquery.treetable.js',
              'pages/js/jquery.pagetree.js',)

admin.site.register(Page, PageAdmin)