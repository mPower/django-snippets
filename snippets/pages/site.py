# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from snippets.pages.utils import generate_path


class PageSite(object):
    def __init__(self):
        self._registry = {}

    def register(self, model_class, page_type_class):
        key = '%s.%s' % (model_class._meta.app_label, model_class._meta.model_name)
        if key in self._registry:
            raise Exception('App %s already registered' % model_class._meta.verbose_name_plural)
        self._registry[key] = page_type_class(model_class)

    def unregister(self, model_class):
        key = '%s.%s' % (model_class._meta.app_label, model_class._meta.model_name)
        if key not in self._registry:
            raise Exception('App %s not registered' % model_class._meta.verbose_name_plural)
        del self._registry[key]

    def items(self):
        for k, model in self._registry.items():
            yield (k, model.verbose_name)

    def default_page(self):
        return '~'

    def get(self, key):
        return self._registry.get(key, None)


class PageType(object):
    fields = None

    def __init__(self, model):
        self.verbose_name = model._meta.verbose_name_plural

    def get_path(self, *args, **kwargs):
        raise NotImplementedError('"get_absolute_url" method not implemented')


class TextPageType(PageType):
    def __init__(self, *args, **kwargs):
        self.verbose_name = _('Text page')

    def get_path(self, obj, *args, **kwargs):
        if obj.parent:
            path = generate_path(obj.slug, obj.parent.path)
        else:
            path = '/'
        return path


class RedirectPageType(TextPageType):
    fields = ('slug', ('redirect_to', 'redirect_to_out'))

    def __init__(self, *args, **kwargs):
        self.verbose_name = _('Redirect to')


site = PageSite()
site._registry['~'] = TextPageType()
site._registry['~~'] = RedirectPageType()