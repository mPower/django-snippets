# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey
from snippets.core.models import Published
from snippets.core.fields import RichTextField
from snippets.pages.settings import TEMPLATE_CHOICES, INMENU_CHOICES
from snippets.pages.site import site


class PageBase(MPTTModel, Published):
    """
    Page class
    """
    INMENU_CHOICES = INMENU_CHOICES
    TEMPLATE_CHOICES = TEMPLATE_CHOICES
    title = models.CharField(_('Title'), max_length=255)
    pagetype = models.CharField(_('Page type'), max_length=100, choices=site.items(), default=site.default_page())
    slug = models.SlugField(_('Slug'), max_length=255, blank=True,
                            help_text=_('A slug is a short name which identifies the item'))
    path = models.SlugField(_('Path'), max_length=2000, editable=False, blank=True)
    redirect_to = models.CharField(_('Redirect to'), max_length=2000, blank=True)
    redirect_to_out = models.URLField(_('Redirect to out'), blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    content = RichTextField(_('Content'), blank=True)
    template = models.CharField(_('Template'), max_length=255, choices=TEMPLATE_CHOICES, default=TEMPLATE_CHOICES[0][0])
    in_menu = models.CharField(_('In menu'), max_length=255, blank=True, default=INMENU_CHOICES[0][0])
    in_sitemap = models.BooleanField(_('In sitemap'), blank=True, default=True)

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        abstract = True

    def save(self, *args, **kwargs):
        if self.parent is None:
            if not self.pk:
                try:
                    parent_page = self._meta.model.objects.get(slug='index')
                except self._meta.model.DoesNotExist:
                    pass
                else:
                    self.parent = parent_page
            if self.parent is None:
                self.slug = 'index'
        if self.slug and self.slug != 'index':
            update_path = kwargs.pop('update_path', False)
            if not update_path:
                if self.pk and not self.is_leaf_node():
                    orig = self._meta.model.objects.get(pk=self.pk)
                    if orig.slug != self.slug:
                        for node in self.get_descendants():
                            node.path = node.path.replace('/'+orig.slug+'/', '/'+self.slug+'/')
                            node.save(update_path=True)
        page_type = site.get(self.pagetype)
        if page_type:
            self.path = page_type.get_path(self)
        super(PageBase, self).save(*args, **kwargs)

    def get_absolute_url(self):
        if self.redirect:
            return self.redirect
        return self.path

    @property
    def redirect(self):
        if self.redirect_to:
            return self.redirect_to
        if self.redirect_to_out:
            return self.redirect_to_out
        return None

    def __unicode__(self):
        return self.title


class Page(PageBase):

    class Meta(PageBase.Meta):
        swappable = 'PAGE_MODEL'