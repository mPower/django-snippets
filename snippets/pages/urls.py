# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.conf import settings

# Page patterns.
urlpatterns = patterns("snippets.pages.views",
    url("^(?P<slug>.*)%s$" % ("/" if settings.APPEND_SLASH else ""), "detail", name="page"),
)