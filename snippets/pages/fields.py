# -*- coding: utf-8 -*-
from django import forms

class MultipleChoiceCharField(forms.MultipleChoiceField):
    widget = forms.CheckboxSelectMultiple

    def to_python(self, value):
        if not value:
            return []
        return ';'.join(value)

    def prepare_value(self, value):
        if value is None:
            return []
        if isinstance(value, (str, unicode)):
            return value.split(';')
        return value

    def validate(self, value):
        if not value:
            value = []
        else:
            value = value.split(';')
        super(MultipleChoiceCharField, self).validate(value)