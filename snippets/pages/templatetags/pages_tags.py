# -*- coding: utf-8 -*-
import itertools
from django import template
from django.template.loader import get_template
from django.utils.safestring import mark_safe, SafeText
from mptt.templatetags.mptt_tags import cache_tree_children
from snippets.pages import get_page_model

register = template.Library()
Page = get_page_model()


@register.simple_tag(takes_context=True)
def page_menu_tree(context, *args):
    """
    Page menu
    {% page_menu [page] ["menu_type:default|top|footer etc."]["path_to_template"] [depth] %}
    If page not set, page is root
    """
    template_name = 'menus/menu.html'
    menu_choices = [v for v, l in Page.INMENU_CHOICES]
    menu = menu_choices[0]
    parent_page = None
    depth = None
    for arg in args:
        if isinstance(arg, Page):
            parent_page = arg
        elif isinstance(arg, SafeText):
            var = str(arg)
            if isinstance(var, str):
                if arg in menu_choices:
                    menu = arg
                else:
                    template_name = arg
            elif isinstance(var, str):
                template_name = arg
        elif isinstance(arg, int):
            depth = arg

    if parent_page is None:
        try:
            parent_page = Page.objects.filter(parent=None, slug='index')[:1][0]
        except:
            return ''
    if not isinstance(parent_page, Page):
        return ''

    menu_search = []
    for i in xrange(1, len(menu_choices)+1):
        menu_search.extend([';'.join(v) for v in itertools.combinations(menu_choices, i) if menu in v])

    def _render_node(template, node, context):
        bits = []
        for child in node.get_children():
            if child.in_menu in menu_search:
                bits.append(_render_node(template,child,context))
        context['node'] = node
        context['children'] = mark_safe(''.join(bits))
        return template.render(context)

    nodes_qs = parent_page.get_descendants().filter(is_published=True)
    if depth is not None:
        depth += parent_page.level
        nodes_qs = nodes_qs.filter(level__lte=depth)
    nodes = cache_tree_children(nodes_qs)
    t = get_template(template_name)
    context = template.Context(context)
    bits = []
    for node in nodes:
        if node.in_menu in menu_search:
            bits.append(_render_node(t, node, context))
    return ''.join(bits)


@register.simple_tag(takes_context=True)
def page_menu(context, *args):
    """
    Page menu
    {% page_menu ["menu_type:default|top|footer etc."]["path_to_template"] %}
    If page not set, page is root
    """
    template_name = 'menus/menu.html'
    menu_choices = [v for v, l in Page.INMENU_CHOICES]
    menu = menu_choices[0]
    for arg in args:
        try:
            var = template.Variable(arg).resolve(context)
        except template.VariableDoesNotExist:
            var = str(arg)
        if isinstance(var, str):
            if arg in menu_choices:
                menu = arg
            else:
                template_name = arg
        elif isinstance(var, str):
            template_name = arg

    menu_search = []
    for i in xrange(1, len(menu_choices)+1):
        menu_search.extend([';'.join(v) for v in itertools.combinations(menu_choices, i) if menu in v])

    def _render_node(template, node, context):
        context['node'] = node
        return template.render(context)

    nodes = Page.published.filter(in_menu__in=menu_search)
    t = get_template(template_name)
    context = template.Context(context)
    bits = []
    for node in nodes:
        bits.append(_render_node(t, node, context))
    return ''.join(bits)


@register.simple_tag(takes_context=True)
def page_sitemap(context, *args):
    """
    Page sitemap
    """
    template_name = 'menus/sitemap.html'
    for arg in args:
        try:
            var = template.Variable(arg).resolve(context)
        except template.VariableDoesNotExist:
            var = str(arg)
        if isinstance(var, str):
            template_name = arg

    def _render_node(template, node, context):
        bits = []
        for child in node.get_children():
            if child.in_sitemap:
                bits.append(_render_node(template, child, context))
        context['node'] = node
        context['children'] = mark_safe(''.join(bits))
        return template.render(context)

    nodes_qs = Page.published.exclude(slug='')
    nodes = cache_tree_children(nodes_qs)
    t = get_template(template_name)
    context = template.Context(context)
    bits = []
    for node in nodes:
        if node.in_sitemap:
            bits.append(_render_node(t, node, context))
    return ''.join(bits)