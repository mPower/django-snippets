# -*- coding: utf-8 -*-
from django import template
from django.contrib.admin.templatetags.admin_list import result_list

register = template.Library()

@register.inclusion_tag("admin/snippets/pages/page/change_list_results.html")
def tree_result_list(cl):
    rl = result_list(cl)
    results = []
    for row, item in zip(rl['results'], cl.result_list):
        results.append((item.pk, item.parent_id, row))
    rl['results'] = results
    return rl
