# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

if hasattr(settings, 'PAGE_TEMPLATES'):
    TEMPLATE_CHOICES = (
        (0, _('Inherit from parent')),
    )
    TEMPLATE_CHOICES = getattr(settings, 'PAGE_TEMPLATES', [])
else:
    raise Exception(_('Set PAGE_TEMPLATES in settings'))

INMENU_CHOICES = getattr(settings, 'PAGE_MENUS', None)
if INMENU_CHOICES is None:
    INMENU_CHOICES = (
        ('default', _('Main menu')),
    )
