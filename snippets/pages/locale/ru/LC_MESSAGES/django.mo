��          �   %   �      0  0   1     b     j     r     �  
   �     �  	   �  )   �     �  	   �  "   �  "        >     D     I     U     e     �     �     �  	   �     �  �  �  o   t     �     �  Q   
     \     h  ,   �     �  k   �     4     E  ?   ]  ?   �     �     �     �  -     8   D     }  .   �     �  #   �     �                                                                                                           
             	       A slug is a short name which identifies the item Actions Content Fill one of redirect fields In menu In sitemap Inherit from parent Main menu Only one of redirect fields can be filled Page Page type Page with slug "%s" already exists Page with type "%s" already exists Pages Path Redirect to Redirect to out Set PAGE_TEMPLATES in settings Slug Slug field can not be empty Template Text page Title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-20 22:45+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Короткое имя, используемое в URL, идентифицирующее публикацию Действия Содержимое Заполните одно из полей для перенаправления В меню В карте сайта Наследовать от родителя Основное меню Только одно поле для перенаправления может быть заполнено Страницу Тип страницы Страница с путем "%s" уже существует Страница с типом "%s" уже существует Страницы Путь Перенаправление Внешнее перенаправление Не указан PAGE_TEMPLATES в настройках Путь Путь не может быть пустым Шаблон Текстовая страница Заголовок 