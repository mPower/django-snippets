# -*- coding: utf-8 -*-
from django.shortcuts import render, Http404
from snippets.pages import get_page_model

Page = get_page_model()


def detail(request, slug, extra_context=None):
    page = getattr(request, 'page', None)
    if page is None:
        raise Http404
    return render(request, page.template, {'object': page})