# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from django.shortcuts import Http404, HttpResponsePermanentRedirect
from snippets.pages import get_page_model

Page = get_page_model()


class PageMiddleware(object):

    def __init__(self):
        if 'snippets.pages' not in settings.INSTALLED_APPS:
            raise MiddlewareNotUsed

    def process_view(self, request, view_func, view_args, view_kwargs):
        request.page = None
        if request.path_info.startswith(settings.STATIC_URL) or request.path_info.startswith(settings.MEDIA_URL):
            return None
        try:
            page = Page.objects.get(path=request.path_info)
        except Page.DoesNotExist:
            return None
        if not page.is_published:
            raise Http404
        if page.pagetype == '~~' and page.redirect:
            return HttpResponsePermanentRedirect(page.redirect)
        request.page = page
        return None