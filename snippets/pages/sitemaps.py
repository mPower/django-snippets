# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from snippets.pages import get_page_model

Page = get_page_model()

class PageSitemap(Sitemap):
    priority = 0.5
    changefreq = "monthly"

    def items(self):
        return Page.published.all()

    def lastmod(self, obj):
        return obj.pub_date
