# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from snippets.pages import get_page_model
from snippets.pages.fields import MultipleChoiceCharField

Page = get_page_model()


class PageAdminForm(forms.ModelForm):
    in_menu = MultipleChoiceCharField(label=_('In menu'), required=False, choices=Page.INMENU_CHOICES)

    class Meta:
        model = Page

    def __init__(self, *args, **kwargs):
        super(PageAdminForm, self).__init__(*args, **kwargs)
        if 'parent' in self.fields:
            self.fields['parent'].widget = forms.HiddenInput()
            self.fields['parent'].label = ''

    def clean(self):
        cleaned_data = super(PageAdminForm, self).clean()
        slug = cleaned_data.get('slug', None)
        parent = cleaned_data.get('parent', None)
        pagetype = cleaned_data.get('pagetype', None)
        has_redirect_to = 'redirect_to' in cleaned_data
        has_redirect_to_out = 'redirect_to_out' in cleaned_data
        redirect_to = cleaned_data.get('redirect_to', None)
        redirect_to_out = cleaned_data.get('redirect_to_out', None)

        if pagetype in ('~', '~~') and 'slug' in self.fields and not slug:
            self._errors['slug'] = self.error_class([_('Slug field can not be empty')])
            del cleaned_data['slug']

        if pagetype == '~~' and self.instance.pk \
                and has_redirect_to and has_redirect_to_out \
                and not redirect_to and not redirect_to_out:
            raise forms.ValidationError(_('Fill one of redirect fields'))

        if redirect_to and redirect_to_out:
            raise forms.ValidationError(_('Only one of redirect fields can be filled'))

        if slug and parent:
            qs = self.Meta.model.objects.all()
            if self.instance.pk:
                qs = qs.exclude(pk=self.instance.pk)
            try:
                exist_page = qs.get(slug=slug, parent_id=parent)
            except Page.DoesNotExist:
                pass
            else:
                self._errors['slug'] = self.error_class([_('Page with slug "%s" already exists') % slug])
                del cleaned_data['slug']

        if 'pagetype' in self.fields and pagetype and pagetype not in ('~', '~~'):
            qs = Page.objects.filter(pagetype=pagetype)
            if self.instance.pk:
                qs = qs.exclude(pk=self.instance.pk)
            if qs.count():
                self._errors['pagetype'] = self.error_class([_('Page with type "%s" already exists') % dict(Page.PAGETYPE_CHOICES).get(pagetype)])
                del cleaned_data['pagetype']
        return cleaned_data