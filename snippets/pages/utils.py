# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


def path_to_slug(path):
    path = path.strip("/") if settings.APPEND_SLASH else path.lstrip("/")
    return path


def slug_to_path(slug):
    path = '/%s' % slug
    if settings.APPEND_SLASH and slug:
        path += '/'
    return path


def generate_path(slug, parent_path):
    if settings.APPEND_SLASH:
        path = parent_path + slug + '/'
    else:
        if parent_path == '/':
            path = parent_path + slug
        else:
            path = parent_path + '/' + slug
    return path


def get_page_model():
    """
    Returns the Page model that is active in this project.
    """
    from django.db.models import get_model

    try:
        app_label, model_name = settings.PAGE_MODEL.split('.')
    except ValueError:
        raise ImproperlyConfigured("PAGE_MODEL must be of the form 'app_label.model_name'")
    page_model = get_model(app_label, model_name)
    if page_model is None:
        raise ImproperlyConfigured("PAGE_MODEL refers to model '%s' that has not been installed" % settings.PAGE_MODEL)
    return page_model
