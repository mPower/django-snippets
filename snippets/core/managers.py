# -*- coding: utf-8 -*-
from django.db import models
from django.utils.timezone import now

class PublishedManager(models.Manager):
    """
        Filters out all unpublished and items with a publication date in the future
    """
    def get_query_set(self):
        return super(PublishedManager, self).get_query_set() \
            .filter(is_published=True) \
            .filter(pub_date__lte=now()) \
            .filter(models.Q(expiry_date__gte=now()) | models.Q(expiry_date__isnull=True))