# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from snippets.core.managers import PublishedManager
from snippets.core.fields import RichTextField

class Slugged(models.Model):
    """
    Slugged mixin
    """

    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255,
                            help_text=_('A slug is a short name which identifies the item'))

    class Meta:
        abstract = True
        ordering = ('-title', )

    def __unicode__(self):
        return self.title

class Published(models.Model):
    """
    Published mixin
    """
    is_published = models.BooleanField(_('Published'), default=False)
    pub_date = models.DateTimeField(_('Publication date'), default=now)
    expiry_date = models.DateTimeField(_("Expires on"),
                                       help_text=_("Won't be shown after this time"),
                                       blank=True, null=True)

    published = PublishedManager()
    objects = models.Manager()

    class Meta:
        abstract = True
        ordering = ('-pub_date', )


class TimeStamped(models.Model):
    """
    TimeStamped mixin
    """
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True

class Description(models.Model):
    """
    Description mixin
    """
    excerpt = models.TextField(_('Excerpt'), blank=True)
    content = RichTextField(_('Content'), blank=True)

    class Meta:
        abstract = True

class Publication(Slugged, Published, TimeStamped, Description):
    """
    Base publication class
    """

    class Meta:
        abstract = True