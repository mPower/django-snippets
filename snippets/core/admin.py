from functools import update_wrapper
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.utils.text import force_text
from django.utils.html import escape
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.contrib import admin
from django.contrib.admin.util import unquote
from django.shortcuts import Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from snippets.seo.admin import MetaDataInline

csrf_protect_m = method_decorator(csrf_protect)

class PublicationAdmin(admin.ModelAdmin):
    """
        Admin for publication
    """
    date_hierarchy = 'pub_date'
    list_display = ('title', 'slug', 'is_published', 'pub_date')
    list_display_links = ('title',)
    list_filter = ('is_published', )
    search_fields = ['title', 'excerpt', 'content']
    prepopulated_fields = {'slug': ('title',)}
    inlines = [MetaDataInline]

    actions = ['make_published', 'make_unpublished']

    save_as = True
    save_on_top = True

    def get_queryset(self, request):
        return self.model.objects.all()

    def make_published(self, request, queryset):
        """
            Marks selected items items as published
        """
        rows_updated = queryset.update(is_published=True)
        self.message_user(request, ungettext('%(count)d sitem was published',
                                             '%(count)d items were published',
                                             rows_updated) % {'count': rows_updated})
    make_published.short_description = _('Publish selected items')

    def make_unpublished(self, request, queryset):
        """
            Marks selected items items as unpublished
        """
        rows_updated = queryset.update(is_published=False)
        self.message_user(request, ungettext('%(count)d item was unpublished',
                                             '%(count)d items were unpublished',
                                             rows_updated) % {'count': rows_updated})
    make_unpublished.short_description = _('Unpublish selected items')

class MPTTOrderingMixin(object):
    def get_urls(self):
        from django.conf.urls import patterns, url

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name
        urlpatterns = super(MPTTOrderingMixin, self).get_urls()

        urlpatterns = patterns('',
           url(r'^(\d+)/moveup/$',
               wrap(self.move_up),
               name='%s_%s_moveup' % info),
           url(r'^(\d+)/movedown/$',
               wrap(self.move_down),
               name='%s_%s_movedown' % info),
           ) + urlpatterns
        return urlpatterns

    @csrf_protect_m
    @transaction.atomic
    def move_up(self, request, object_id, extra_context=None):
        opts = self.model._meta
        obj = self.get_object(request, unquote(object_id))

        if obj is None:
            raise Http404(
                _('%(name)s object with primary key %(key)r does not exist.') %
                {'name': force_text(opts.verbose_name), 'key': escape(object_id)}
            )

        prev_sibling = obj.get_previous_sibling()
        if prev_sibling:
            obj.move_to(prev_sibling, position='left')
        post_url = reverse('admin:%s_%s_changelist' %
                           (opts.app_label, opts.model_name),
                           current_app=self.admin_site.name)
        return HttpResponseRedirect(post_url)

    @csrf_protect_m
    @transaction.atomic
    def move_down(self, request, object_id, extra_context=None):
        opts = self.model._meta
        obj = self.get_object(request, unquote(object_id))

        if obj is None:
            raise Http404(
                _('%(name)s object with primary key %(key)r does not exist.') %
                {'name': force_text(opts.verbose_name), 'key': escape(object_id)}
            )

        next_sibling = obj.get_next_sibling()
        if next_sibling:
            obj.move_to(next_sibling, position='right')
        post_url = reverse('admin:%s_%s_changelist' %
                           (opts.app_label, opts.model_name),
                           current_app=self.admin_site.name)
        return HttpResponseRedirect(post_url)