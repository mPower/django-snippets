# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models

__all__ = ['ImageField', 'FileField', 'RichTextField']

if 'tinymce' in settings.INSTALLED_APPS:
    from tinymce.models import HTMLField
    RichTextField = HTMLField
else:
    RichTextField = models.TextField

if 'filebrowser' in settings.INSTALLED_APPS:
    from filebrowser.fields import FileBrowseField
    # ImageField = type('ImageField', (FileBrowseField,), dict(format='image'))
    # FileField = type('FileField', (FileBrowseField,), dict(format='file'))

    class ImageField(FileBrowseField):
        def __init__(self, *args, **kwargs):
            directory = kwargs.pop('upload_to', '')
            super(ImageField, self).__init__(*args, **kwargs)
            self.format = 'image'
            self.directory = directory

    class FileField(FileBrowseField):
        def __init__(self, *args, **kwargs):
            directory = kwargs.pop('upload_to', '')
            super(FileField, self).__init__(*args, **kwargs)
            self.format = 'file'
            self.directory = directory
else:
    ImageField = models.ImageField
    FileField = models.FileField

# South integration
try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^snippets\.core\.fields\.ImageField"])
    add_introspection_rules([], ["^snippets\.core\.fields\.FileField"])
except ImportError:
    pass