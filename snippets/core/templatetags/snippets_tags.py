# -*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.filter
def img_version(img_obj, version_name):
    try:
        url = img_obj.version_generate(version_name).url
    except:
        url = img_obj.url
    return url