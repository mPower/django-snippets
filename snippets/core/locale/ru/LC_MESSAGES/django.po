# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-02-17 12:50+0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"

#: admin.py:29
#, fuzzy, python-format
msgid "%(count)d sitem was published"
msgid_plural "%(count)d items were published"
msgstr[0] "%(count)d новость была опубликовано"
msgstr[1] "%(count)d новости было опубликовано"

#: admin.py:32
#, fuzzy
msgid "Publish selected items"
msgstr "Опубликовать выбранные новости"

#: admin.py:39
#, fuzzy, python-format
msgid "%(count)d item was unpublished"
msgid_plural "%(count)d items were unpublished"
msgstr[0] "%(count)d новость отменена публикация"
msgstr[1] "%(count)d новости отменена публикация"

#: admin.py:42
#, fuzzy
msgid "Unpublish selected items"
msgstr "Отменить публикацию выбранных новостей"

#: models.py:13
msgid "Title"
msgstr "Заголовок"

#: models.py:14
msgid "Slug"
msgstr "Путь"

#: models.py:15
msgid "A slug is a short name which identifies the item"
msgstr "Короткое имя, используемое в URL, идентифицирующее публикацию"

#: models.py:28
msgid "Published"
msgstr "Опубликовано"

#: models.py:29
msgid "Publication date"
msgstr "Дата публикации"

#: models.py:30
msgid "Expires on"
msgstr "Истекает"

#: models.py:31
msgid "Won't be shown after this time"
msgstr "Не будет отображаться после указанного времени"

#: models.py:56
msgid "Excerpt"
msgstr "Выдержка"

#: models.py:57
msgid "Content"
msgstr "Содержимое"