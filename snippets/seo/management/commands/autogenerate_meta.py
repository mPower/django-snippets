# -*- coding: utf-8 -*-
from django.db.models import get_model
from django.core.management.base import BaseCommand, CommandError
from snippets.seo.models import MetaData

class Command(BaseCommand):
    def handle(self, *args, **options):
        title_fn = options.get('title_fn','title')
        for model in args:
            app_label, model_name = model.split('.')
            model_class = get_model(app_label, model_name)
            for item in model_class.objects.all():
                item_title = getattr(item, title_fn, '')
                mdata = MetaData(_meta_title=item_title, content_object=item)
                print mdata._meta_title
                mdata.save()