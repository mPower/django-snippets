# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _

class MetaDataBase(models.Model):
    """
    Abstract model that provides meta data for content.
    """

    _meta_title = models.CharField(_("Title"), null=True, blank=True,
                                   max_length=500,
                                   help_text=_("Optional title to be used in the HTML title tag. "
                                               "If left blank, the main title field will be used."))
    keywords = models.CharField(_('Keywords'), max_length=100, blank=True)
    description = models.TextField(_("Description"), blank=True)

    class Meta:
        abstract = True

    def meta_title(self):
        """
        Accessor for the optional ``_meta_title`` field, which returns
        the string version of the instance if not provided.
        """
        return self._meta_title or str(self)

class MetaData(MetaDataBase):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def meta_title(self):
        return self._meta_title or str(self.content_object)

    def __unicode__(self):
        return self.meta_title()

    class Meta:
        verbose_name = u'Метаданные'
        verbose_name_plural = u'Метаданные'
        unique_together   = ('content_type', 'object_id')