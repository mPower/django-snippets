# -*- coding: utf-8 -*-
from django.contrib.contenttypes.generic import GenericStackedInline
from snippets.seo.models import MetaData

class MetaDataInline(GenericStackedInline):
    model = MetaData
    extra = 1
    max_num = 1
    classes = ('grp-collapse grp-closed',)
    inline_classes = ('grp-collapse grp-open',)