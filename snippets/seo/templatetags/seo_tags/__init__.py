# -*- coding: utf-8 -*-
from django import template
from django.contrib.contenttypes.models import ContentType
from snippets.seo.models import MetaData

register = template.Library()

@register.assignment_tag
def seo_metadata(*args):
    if not args:
        return ''
    mdata = None
    for arg in args:
        if not arg:
            continue
        try:
            obj_type = ContentType.objects.get_for_model(arg)
        except ContentType.DoesNotExist:
            pass
        else:
            try:
                mdata = MetaData.objects.get(content_type=obj_type, object_id=arg.pk)
            except MetaData.DoesNotExist:
                pass
            else:
                break
    return mdata
